import Vue from 'vue'
import VueRouter from 'vue-router'
import MainView from '../layouts/MainLayout.vue'
import AuthView from '../layouts/AuthLayout.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: MainView,
    children: [
      { path: 'dashboard', component: () => import('../views/DashboardPage.vue') },
      { path: 'post', component: () => import('../views/PostPage.vue') }
    ]
  },
  {
    path: '/:catchAll(.*)*',
    name: 'auth',
    component: AuthView,
    children: [
      { path: 'login', component: () => import('../views/LoginPage.vue') },
      { path: 'register', component: () => import('../views/RegisterPage.vue') }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
