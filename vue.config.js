const isProduction = process.env.NODE_ENV === 'production';
const CompressionPlugin = require("compression-webpack-plugin")

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  chainWebpack: config => {
    if (isProduction) {
      // удаляем предварительную загрузку
      config.plugins.delete('preload');
      config.plugins.delete('prefetch');
      // Сжатый код
      config.optimization.minimize(true);
      //
      config.performance.hints(false);
      // разделить код
      config.optimization.splitChunks({
        chunks: 'all',
        minSize: 10000,
        maxSize: 240000,
      });
    }
  },
  configureWebpack: config => {
    if (isProduction) {
      new CompressionPlugin({
        test: / \. js $ | \ .html $ |. \ css /, // соответствие имени файла
        threshold: 10240, // Сжать данные более 10k
        deleteOriginalAssets: false // Не удалять исходные файлы
      })
    }
  }
}